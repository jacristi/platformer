﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseMaxHealth : Effect
{
     public override void Apply(PlayerController targetScript) {
        // Apply effect to the specified target
        targetScript.AdjustMaxHealth(effectAmount);
    }

    public override void Remove(PlayerController targetScript) {
        
    }
}
