﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeSlowFall : Effect
{
    public override void Apply(PlayerController targetScript) {
        // Apply effect to the specified target
        targetScript.AdjustChargeFallSpeed(0.8f);
    }

    public override void Remove(PlayerController targetScript) {
        // Remove effect from the specified target
        targetScript.AdjustChargeFallSpeed(1.0f);
    }
}
