﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageBoost : Effect
{
    public override void Apply(PlayerController targetScript) {
        // Apply effect to the specified target
        targetScript.AdjustBonusDamage(effectAmount);
    }

    public override void Remove(PlayerController targetScript) {
        // Remove effect from the specified target
        targetScript.AdjustBonusDamage(-effectAmount);
    }
}
