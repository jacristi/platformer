﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour
{
    [SerializeField] protected int effectAmount = 1;
    [SerializeField] protected float effectTime = 5.0f;
    [SerializeField] protected Color effectColor = Color.white;

    public virtual void Apply(PlayerController targetScript) {
        // Apply effect to the specified target
    }

    public virtual void Remove(PlayerController targetScript) {
        // Remove effect from the specified target
    }

    public virtual float GetEffectDuration() {
        return effectTime;
    }

    public virtual Color GetEffectColor() {
        return effectColor;
    }
}
