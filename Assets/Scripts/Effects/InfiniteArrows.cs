﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteArrows : Effect
{
    public override void Apply(PlayerController targetScript) {
        // Apply effect to the specified target
        targetScript.EnableInfiniteArrows(true);
        targetScript.AdjustAttackRate(7f);
    }

    public override void Remove(PlayerController targetScript) {
        // Remove effect from the specified target
        targetScript.EnableInfiniteArrows(false);
        targetScript.ResetAttackRate();
    }
}
