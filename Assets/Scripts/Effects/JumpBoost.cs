﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBoost : Effect
{
    public override void Apply(PlayerController targetScript) {
        // Apply effect to the specified target
        targetScript.AdjustJumpHeight(effectAmount);
        targetScript.AdjustJumpOnDamage(2);
    }

    public override void Remove(PlayerController targetScript) {
        // Remove effect from the specified target
        targetScript.AdjustJumpHeight(-effectAmount);
        targetScript.AdjustJumpOnDamage(-2);
    }
}
