﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoost : Effect
{
    public override void Apply(PlayerController targetScript) {
        // Apply effect to the specified target
        targetScript.AdjustSpeed(effectAmount);
    }

    public override void Remove(PlayerController targetScript) {
        // Remove effect from the specified target
        targetScript.AdjustSpeed(-effectAmount);
    }
}
