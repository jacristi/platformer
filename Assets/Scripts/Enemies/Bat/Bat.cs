﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : Enemy
{
    // Loot Prefabs
    [SerializeField] private GameObject heartPrefab;

    protected override void dropLoot() {
        // Chance to drop a coin, arrow, heart, or nothing
        if (Random.value > 0.2) {
            Instantiate(heartPrefab, transform.position, transform.rotation);
        } else {
            // No Loot
        }
    }
}
