﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Component Vars
    protected Animator anim;
    protected Rigidbody2D rb;
    protected Collider2D coll;

    // Player Vars
    protected GameObject player;
    protected PlayerController playerScript;

    // Movement 'AI' Vars
    protected RaycastHit2D groundInfo;
    protected RaycastHit2D wallInfo;
    protected Vector2 forwardCheckDir;
    [SerializeField] protected LayerMask ground;
    [SerializeField] protected Transform rayDetector;
    protected bool collisionWithEnemy = false;

    [SerializeField] protected bool facingLeft;
    [SerializeField] protected float speed = 2;
    [SerializeField] protected float startingX;
    [SerializeField] protected float startingY;
    [SerializeField] protected float wanderDist = 10;

    // Attack effect(s)
    [SerializeField] protected GameObject attackEffect;

    // Health bars
    [SerializeField] protected HealthBar healthBar;
    protected RectTransform healthBarTransform;

    // Inspector Stat Vars
    [SerializeField] protected int health = 2;
    [SerializeField] protected int maxHealth = 2;
    [SerializeField] protected int attackPower = 1;
    [SerializeField] protected bool dropsLoot = true;
    [SerializeField] protected bool flyingEnemy = false;
    [SerializeField] protected float corpseLingerTime = 2.0f;
    [SerializeField] protected float attackRate = 2f;
    [SerializeField] protected float attackDelayAfterDamage = 1f;

    // Private/Protected Vars
    protected float nextAttackTime;
    protected bool attackDelayed = false;
    protected bool wasKilled = false;

    // State
    protected enum State {idle, walking, attacking, damaged, dead}
    protected State state = State.walking;

    // Audio Sources
    [SerializeField] protected AudioSource audioDamaged;
    [SerializeField] protected AudioSource audioDeath;
    [SerializeField] protected AudioSource audioAttack;

    protected virtual void Start() {
        // Set components
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        coll = GetComponent<Collider2D>();

        startingX = transform.position.x;
        startingY = transform.position.y;

        healthBar.setMaxHealth(maxHealth);
        healthBarTransform = healthBar.GetComponent<RectTransform>();
        showHideHealthBar();

        player = GameObject.FindWithTag("Player");
        if (player != null) { // For any monsters not used in a level (e.g. bat in StartScene)
            playerScript = player.GetComponent<PlayerController>();
        }

    }

    protected virtual void Update() {
        if (state != State.dead) {
            if (Time.time >= nextAttackTime) {
                attackDelayed = false;
            }
            HandleMovement();
        } else if (flyingEnemy) {
            if (coll != null){
                if (coll.IsTouchingLayers(ground)) {
                    if (rb != null) {
                        Destroy(rb);
                    }
                    Destroy(coll);
                }
            } else {
                if (rb != null) {
                    rb.velocity = new Vector2(0, rb.velocity.y);
                }

            }
        }
        AnimationState();
        anim.SetInteger("state", (int)state);
    }

    public virtual void DealDamageToPlayer() {
        int dir;
        if (player.transform.position.x > transform.position.x) {
            dir = 1;
        } else {
            dir = -1;
        }
        playerScript.TakeDamage(attackPower, dir);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "Enemy") {
            collisionWithEnemy = true;
        }
        // Move damage dealing to player from playerScript to here
    }

    private bool PastWanderDistance() {
        if (transform.position.x < startingX - wanderDist) {
            facingLeft = false;
        }
        if (transform.position.x > startingX + wanderDist) {
            facingLeft = true;
        }
        return false;
    }

    protected virtual void HandleMovement() {

        forwardCheckDir = facingLeft ? Vector2.left : Vector2.right;
        wallInfo = Physics2D.Raycast(rayDetector.position, forwardCheckDir, .1f, ground);
        groundInfo = Physics2D.Raycast(rayDetector.position, Vector2.down, 1f, ground);

        if (collisionWithEnemy) {
            // if colliding with an enemy, only turn around for the update
            facingLeft = !facingLeft;
            collisionWithEnemy = false;
        } else {
            // Check for ledges below
            if (groundInfo.collider == false && !flyingEnemy) {
                facingLeft = !facingLeft;

            // Check for walls ahead
            } else if (wallInfo == true) {
                facingLeft = !facingLeft;
            }

            // Check if past specified wander distance
            if (transform.position.x < startingX - wanderDist) {
                facingLeft = true;
            } else if (transform.position.x > startingX + wanderDist) {
                facingLeft = false;
            }

            // Move
            if (state == State.attacking){
                // Face player
                if (player.transform.position.x < transform.position.x) {
                    transform.localScale = new Vector2(-1, 1);
                } else {
                    transform.localScale = new Vector2(1, 1);
                }
                // Stop moving
                rb.velocity = new Vector2(0, 0);
            } else if (facingLeft) {
                rb.velocity = new Vector2(speed, 0);
                transform.localScale = new Vector2(1, 1);
            } else {
                rb.velocity = new Vector2(-speed, 0);
                transform.localScale = new Vector2(-1, 1);

            }
        }
    }

    protected virtual void AnimationState () {
    }

    protected void showHideHealthBar() {
        // Do not display enemy healthbar if health if at maxHealth
        if (health == maxHealth) {
            hideHealthBar();
        } else {
            showHealthBar();
        }
    }

    protected void hideHealthBar() {
        healthBarTransform.localScale = new Vector3(0, 0, 0);
    }

    protected void showHealthBar() {
        healthBarTransform.localScale = new Vector3(1, 1, 1);
    }

    public void TakeDamage(int damage, int dir) {
        updateHealth(-damage);
        PlayAudioDamaged();
    }

    public void updateHealth(int incr) {
        // Adjust health value
        health += incr;

        if (incr < 0) {
            // On damage delay next attack time
            if (!attackDelayed) {
                nextAttackTime = Time.time + attackDelayAfterDamage;
                attackDelayed = true;
            }

        }

        // Check against Max Health
        if (health > maxHealth) {
            health = maxHealth;
        }

        // Update health display
        if (healthBar != null) {
        healthBar.adjustHealth(health);
        }

        // Do not display enemy healthbar if health if at maxHealth
        showHideHealthBar();

        // Check for Death
        if (health <= 0) {
            Death();
        }
    }

    protected virtual void DestroyComponents() {
        if (rb != null) {
            Destroy(rb);
        }
        if (coll != null) {
            Destroy(coll);
        }
        if (healthBar != null) {
            hideHealthBar();
        }
    }

    public virtual void Death() {
        // Change state to dead and remove collider and rigid body

        state = State.dead;
        wasKilled = true;
        attackEffect.gameObject.SetActive(false);
        if (flyingEnemy) {
            // Let flying enemies fall to the ground
            rb.velocity = new Vector2(0, -8);
        } else {
            DestroyComponents();
        }

        // Destroy after x seconds
        PlayAudioDeath();
        Destroy(gameObject, corpseLingerTime);

    }

    protected virtual void dropLoot() {
        // Implement in child
    }

    private void OnDestroy() {
        attackEffect.gameObject.SetActive(false);
        // Quick fix to not spawn loot on application quitting and only when killed/dead
        if (wasKilled && dropsLoot == true) {
            dropLoot();
        }
    }

    public void PlayAudioDamaged() {
        if (audioDamaged != null) {
            audioDamaged.Play();
        }
    }

    public void PlayAudioDeath() {
        if (audioDeath != null) {
            audioDeath.Play();
        } else {
            PlayAudioDamaged();
        }

    }

    public void PlayAudioAttack() {
        if (audioAttack != null) {
            audioAttack.Play();
        }
    }

    public virtual void Attack() {
        if (state != State.dead) {
            if (Time.time >= nextAttackTime) {
                state = State.attacking;
                nextAttackTime = Time.time + 1f/attackRate;
            }
        }
    }

    public virtual void StartAttackEffect(){
        if (state != State.dead){
            attackEffect.gameObject.SetActive(true);
        }
        PlayAudioAttack();
    }

    public void EndAttackEffect() {
        attackEffect.gameObject.SetActive(false);
        state = State.walking;
    }

    public void SetWanderDistance(float val) {
        wanderDist = val;
    }

    public bool WasKilled() {
        return wasKilled;
    }

}
