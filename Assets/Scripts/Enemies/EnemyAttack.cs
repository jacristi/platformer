﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    [SerializeField] private bool blocksProjectiles = false;

    private Enemy enemyScript;

    private void Start() {
        enemyScript = enemy.GetComponent<Enemy>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Projectile" && blocksProjectiles) {
            Destroy(other.gameObject);
        }
    }

    public void TriggerAttackOnPlayer() {
        enemyScript.DealDamageToPlayer();
    }
}
