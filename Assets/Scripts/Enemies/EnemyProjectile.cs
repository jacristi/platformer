﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    // Component Vars
    public Rigidbody2D rb;
    private Collider2D coll;
    private int damageToDeal = 1;

    protected GameObject player;
    protected PlayerController playerScript;

    [SerializeField] private LayerMask ground;

    void Start() {
        coll = GetComponent<Collider2D>();
        player = GameObject.FindWithTag("Player");
        if (player != null) { // For any monsters not used in a level (e.g. bat in StartScene)
            playerScript = player.GetComponent<PlayerController>();
        }
    }

    void Update() {
        // Destroy if touching ground
        if (coll.IsTouchingLayers(ground)) {
            Destroy(gameObject);
        };
    }

    private void OnTriggerEnter2D(Collider2D other) {

        // Damage player and destroy self
        if (other.gameObject.tag == "Player") {

            int dir;
            if (other.transform.position.x > transform.position.x) {
                dir = 1;
            } else {
                dir = -1;
            }
            playerScript.TakeDamage(damageToDeal, dir);
            Destroy(gameObject);

        // Destroy if collides with another projectile
        } else if (other.gameObject.tag == "Projectile") {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }

    public void SetDamageToDeal(int val) {
        damageToDeal = val;
    }

}
