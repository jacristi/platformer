﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSkeleton : Enemy
{
    // Loot Objects
    [SerializeField] private GameObject bannerPrefab;
    [SerializeField] private GameObject firePoint;
    [SerializeField] private GameObject projectilePrefab;

    protected int attacksCounter = 0;

    protected void delayNextAttack(float val) {
        nextAttackTime = Time.time + val;
        attackDelayed = true;
    }

    protected override void dropLoot() {
        if (health <= 0) {

            var pos = new Vector2(transform.position.x, transform.position.y+2);
            Instantiate(bannerPrefab, pos, transform.rotation);

            }
        }

    private void SpawnProjectile() {
        bool playerToLeft = player.transform.position.x < transform.position.x;
        float dir = playerToLeft ? -1 : 1;
        float rot = playerToLeft ? 1 : 0;
        GameObject projectile = Instantiate(projectilePrefab, transform.position, Quaternion.Euler(0, rot*180, 0));
        Rigidbody2D projRB = projectile.GetComponent<Rigidbody2D>();
        projRB.velocity = transform.right * dir * 10;
        projectile.GetComponent<EnemyProjectile>().SetDamageToDeal(2);
    }

    public override void StartAttackEffect(){
        if (attacksCounter >= 3) {
            delayNextAttack(1f);
            attacksCounter = 0;
        } else {
            if (attacksCounter == 0){
                // Main sword attack only on first of 3 swings
                attackEffect.gameObject.SetActive(true);
            }
            PlayAudioAttack();
            // Spawn projectiles in both directions
            SpawnProjectile();
            attacksCounter += 1;
        }

    }
}
