﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeleton : Enemy
{
    // Loot Objects
    [SerializeField] private GameObject coinPrefab;
    [SerializeField] private GameObject arrowPrefab;

    protected override void dropLoot() {
        if (health <= 0) {
            // Chance to drop a coin, arrow, heart, or nothing
            if (Random.value > 0.85) {
                Instantiate(coinPrefab, transform.position, transform.rotation);
            } else if (Random.value > 0.65) {
                Instantiate(arrowPrefab, transform.position, transform.rotation);
            } else {
                // No Loot
            }
        }
    }

}
