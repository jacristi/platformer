﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonSightRange : MonoBehaviour
{
    public GameObject enemy;

    // Try to trigger attack while player is in range
    private void OnTriggerStay2D(Collider2D other) {
        var enemyScript = enemy.GetComponent<Enemy>();
        if (other.gameObject.tag == "Player") {
            enemyScript.Attack();
        }
    }
}
