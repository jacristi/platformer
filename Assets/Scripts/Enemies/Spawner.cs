﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private Animator anim;
    [SerializeField] protected int maxObjects = 6;
    [SerializeField] protected int health = 2;
    [SerializeField] protected int maxHealth = 2;
    [SerializeField] protected int objWanderDistance = 10;
    [SerializeField] protected GameObject objectPrefab;

    protected enum State {fullHealth, halfHealth}
    protected State state = State.fullHealth;
    protected GameObject objSpawned = null;

    [SerializeField] protected AudioSource audioDamaged;

    protected virtual void Start()
    {
        anim = GetComponent<Animator>();
    }

    protected virtual void Update()
    {
        // Destroy if already spawned allotted amount enemies
        if (maxObjects <= 0) {
            state = State.halfHealth;
            Destroy(gameObject, 1f);
        }
        if (health <= maxHealth/2) {
            state = State.halfHealth;
        }
        if (health <= 0) {
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;
            Destroy(gameObject, 1f);
        }
        anim.SetInteger("state", (int)state);
    }

    public virtual void TakeDamage(int damage, int dir) {
        updateHealth(-damage);
        PlayAudioDamaged();
    }

    public void updateHealth(int incr) {
        health += incr;
    }

    private void SpawnObject() {
        if (maxObjects > 0 && objSpawned == null) {
            objSpawned = Instantiate(objectPrefab, transform.position, transform.rotation);
            objSpawned.GetComponent<Enemy>().SetWanderDistance(objWanderDistance);
            maxObjects -= 1;
        }
    }

    protected virtual void OnBecameVisible() {
        // Spawn enemy when spawner becomes visible
        SpawnObject();
    }

    protected virtual void OnDestroy() {
        if (health == 0) {
            DropLoot();
        }
    }

    protected virtual void DropLoot() {
        // Define in children
    }

    public virtual void PlayAudioDamaged() {
        audioDamaged.Play();
    }
}
