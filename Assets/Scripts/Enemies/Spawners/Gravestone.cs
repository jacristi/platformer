﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravestone : Spawner
{
    [SerializeField] private GameObject coinPrefab;

    protected override void DropLoot() {
        Instantiate(coinPrefab, transform.position, transform.rotation);
    }
}
