﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : Item
{
    public override void collectItem() {
        playerScript.addArrows(itemValue);
        base.collectItem();
        Destroy(gameObject, 1f);
    }
}
