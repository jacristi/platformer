﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Item
{
    public override void collectItem() {
        playerScript.addCoins(itemValue);
        base.collectItem();
        Destroy(gameObject, 1f);
    }
}
