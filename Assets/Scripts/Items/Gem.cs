﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : Item
{
    [SerializeField] private GameObject effect;

    [SerializeField] private bool canRespawn = true;
    [SerializeField] private float respawnTime = 5f;

    public override void collectItem() {
        bool canCollect = playerScript.ApplyEffect(effect);
        if (canCollect == true) {
            base.collectItem();
            float dur = effect.GetComponent<Effect>().GetEffectDuration();
            if (canRespawn){
                Invoke("Reactivate", respawnTime);
            } else {
                Destroy(gameObject, 1f);
            }
        }
    }
    public void Reactivate() {
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<BoxCollider2D>().enabled = true;
    }
}
