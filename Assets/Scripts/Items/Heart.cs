﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : Item
{
    public override void collectItem() {
        playerScript.updateHealth(itemValue);
        base.collectItem();
        Destroy(gameObject, 1f);
    }
}
