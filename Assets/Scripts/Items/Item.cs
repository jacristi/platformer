﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Item : MonoBehaviour
{
    protected GameObject player;
    protected PlayerController playerScript;

    [SerializeField] protected int itemValue = 1;
    [SerializeField] protected AudioSource audioCollect;
    [SerializeField] protected string floatTextValue = "";
    [SerializeField] protected FloatText floatText;
    [SerializeField] protected float floatTextDuration = 0.5f;
    protected FloatText floatTextScript;


    protected virtual void Start()
    {
        // Get player component
        player = GameObject.FindWithTag("Player");
        playerScript = player.GetComponent<PlayerController>();

        if (floatText != null) {
            floatTextScript = floatText.GetComponent<FloatText>();
            floatTextScript.Setup(floatTextValue);
            floatTextScript.Hide();
        }
    }

    public virtual void collectItem(){
        // Should be extended by child
        audioCollect.Play();
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
        if (floatText != null) {
            floatTextScript.Show(floatTextDuration);
            Destroy(floatText, floatTextDuration+0.3f);
        }
    }
}
