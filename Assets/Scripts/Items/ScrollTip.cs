﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollTip : Item
{
    public override void collectItem() {
        playerScript.updateHealth(1);
        audioCollect.Play();
        if (floatText != null) {
            floatTextScript.Show(floatTextDuration);
        }
    }
}
