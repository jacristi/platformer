﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Merchant : MonoBehaviour
{
    private GameObject player;
    private PlayerController playerScript;
    private bool playerCanBuy = false;
    [SerializeField] private GameObject merchantOverlay;


    private void Start() {
        // Get player component
        player = GameObject.FindWithTag("Player");
        playerScript = player.GetComponent<PlayerController>();
    }

    private void Update() {
        if (playerCanBuy == true) {
            if (Input.GetKeyDown(KeyCode.Z)) {
                playerScript.BuyArrows(2, 1);
            }
            if (Input.GetKeyDown(KeyCode.X)) {
                playerScript.BuyHearts(3, 1);
            }
        }
    }

    public void PlayerInRange(bool val) {
        playerCanBuy = val;
        merchantOverlay.gameObject.SetActive(val);
    }
}
