﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerController : MonoBehaviour
{
    // Component/GameObject Variables
    private Rigidbody2D rb;
    private Animator anim;
    private Collider2D coll;
    private SpriteRenderer spriteRenderer;

    // Fire Point Objects/Attributes
    public GameObject firePoint;
    private SpriteRenderer firePointSpriteRenderer;
    private Animator firePointAnimator;
    [SerializeField] private Color chargeUpColor1 = Color.white;
    [SerializeField] private Color chargeUpColor2 = Color.white;

    // Projectile(s)
    public GameObject projectilePrefab;

    // Health Bar
    public HealthBar healthBar;
    private RectTransform healthBarTransform;

    // Layers
    [SerializeField] private LayerMask ground;

    // States
    private enum State {idle, running, jumping, falling, damaged, charging, releasing, dead, climbing}
    [SerializeField] private State state = State.idle;

    // Inspector Variables
    [SerializeField] private float speed = 3;
    [SerializeField] private float jumpHeight = 4;
    [SerializeField] private int jumpOnDamage = 2; // How much damage to deal when jumping on enemies
    [SerializeField] private int baseShotDamage = 1;
    [SerializeField] public bool infiniteArrows = false;
    [SerializeField] private int bonusDamage = 0;
    [SerializeField] private float attackRate = 1f;
    [SerializeField] private int coins = 0;
    [SerializeField] private int arrows = 5;
    [SerializeField] private float knockbackForce = 8f; // How far to be knocked back on damage
    [SerializeField] private int health = 3;
    [SerializeField] private int maxHealth = 3;
    [SerializeField] private float chargeFallSpeed = 1; // How much to affect fall speed while charging
    [SerializeField] private float climbSpeed = 3;

    private bool isPaused = false;
    private int shotCharge = 0;
    private int shotPower = 0;
    private bool m_FacingRight = true;
    private bool canClimb = false;
    private float nextAttackTime;
    private float baseAttackRate;

    // Audio Sources
    [SerializeField] private AudioSource audioFootstep;
    [SerializeField] private AudioSource audioJump;
    [SerializeField] private AudioSource bowCharged;
    [SerializeField] private AudioSource bowCharging;
    [SerializeField] private AudioSource audioShoot;
    [SerializeField] private AudioSource audioDamaged;
    [SerializeField] private AudioSource audioDeath;
    [SerializeField] private AudioSource audioSpendCoin;
    [SerializeField] private AudioSource audioMain;

    // UI
    [SerializeField] private TextMeshProUGUI coinsNumText;
    [SerializeField] private TextMeshProUGUI arrowsNumText;
    [SerializeField] private TextMeshProUGUI hpNumText;
    [SerializeField] private GameObject deathScreen;
    [SerializeField] private GameObject pauseScreen;

    // Effect
    [SerializeField] private GameObject effect;
    private bool effectIsActive = false;
    private Effect effectScript;

    // Stage Save Point
    [SerializeField] private GameObject stageSavePoint;

    private void Awake() {
        isPaused = false;
        Time.timeScale = 1;
        if (PlayerStats.StartAtCheckPoint) {
            transform.position = stageSavePoint.transform.position;
            LoadStats();
        }
        nextAttackTime = Time.time;

    }

    void Start(){
        // Set up required components
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        coll = GetComponent<Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        // Load Stats only if not first scene(s)
        // This will need to change with more levels + load/save progress
        if (SceneManager.GetActiveScene().buildIndex > 1) {
                LoadStats();
            }

        // Initialize UI values
        arrowsNumText.text = arrows.ToString();
        coinsNumText.text = coins.ToString();
        hpNumText.text = health.ToString() + "/" + maxHealth.ToString();

        // Initialize Health bar
        healthBarTransform = healthBar.GetComponent<RectTransform>();
        healthBar.setMaxHealth(maxHealth);
        showHideHealthBar();

        // Initialize firePoint Objects
        firePointSpriteRenderer = firePoint.GetComponent<SpriteRenderer>();
        firePointAnimator = firePoint.GetComponent<Animator>();

        baseAttackRate = attackRate;
    }

    void Update() {

        if (isPaused) { // No updates when paused
            return;
        }
        if (state != State.dead) {

            // Start to charge shot if possible
            if (arrows > 0 && state != State.climbing) {

                // Check against attack rate
                if (Input.GetButtonDown("Fire1") && Time.time >= nextAttackTime) {
                    nextAttackTime = Time.time + 1f/attackRate;
                    state = State.charging;

                } else if (Input.GetButtonUp("Fire1")) {
                    if (state == State.charging) {
                        state = State.releasing;
                        Shoot();
                    }
                    shotCharge = 0;
                }
            }

            if (state != State.damaged && state != State.charging) {
                // Handle input only when not damaged or charging a shot
                if ((canClimb && (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S))) || (state == State.climbing)) {
                    state = State.climbing;
                    HandleMovementWhileClimbing();
                } else {
                    HandleMovement();
                }
            } else if (state == State.damaged) {
                // Handle movement specifically for when damaged
                HandleMovementWhileDamaged();
            } else {
                // Handle movement specifically for when charging a shot
                HandleMovementWhileCharging();
            }

            // Set State
            AnimationState();
            anim.SetInteger("state", (int)state);

        } else {
            Death();
        }
    }
    private void FixedUpdate() {

        if (isPaused) { // No updates when paused
            return;
        }
        // Manage shot charge/power values and firePoint attributes
        if (Input.GetButton("Fire1") && arrows > 0) {
            shotCharge += 1;
            if (shotCharge < 60) {
                shotPower = baseShotDamage;
                adjustSpiteRendererColor(Color.white);
                if (shotCharge > 15) {
                    firePointSpriteRenderer.enabled = true;
                    firePointAnimator.Play("bow_charge_1");
                }
                if (shotCharge == 35) {bowCharging.Play();}
            } else if (shotCharge >= 60 && shotCharge < 120) {
                firePointSpriteRenderer.enabled = true;
                firePointSpriteRenderer.color = chargeUpColor1;
                firePointAnimator.Play("bow_charge_1");
                shotPower = baseShotDamage+1;
                adjustSpiteRendererColor(chargeUpColor1);
                if (shotCharge == 119) {bowCharged.Play();}
            } else if (shotCharge >= 120) {
                shotPower = baseShotDamage+2;
                adjustSpiteRendererColor(chargeUpColor2);
                firePointSpriteRenderer.enabled = true;
                firePointSpriteRenderer.color = chargeUpColor2;
                firePointAnimator.Play("bow_charge_2");
            }
        } else {
            adjustSpiteRendererColor(Color.white);
            firePointSpriteRenderer.color = Color.white;
            firePointSpriteRenderer.enabled = false;
            firePointAnimator.Play("bow_idle");
        }

    }

    private void LateUpdate() {

        // Check for escape to trigger pause/pause menu
        if (Input.GetKeyDown(KeyCode.Escape)) {
            isPaused = !isPaused;
            if (isPaused) {
                Time.timeScale = 0f;
             } else {
                 Time.timeScale = 1f;
             }
            pauseScreen.gameObject.SetActive(!pauseScreen.gameObject.activeSelf);
        }
    }

    public void addCoins(int val) {
        // Add coins to player's total and update display
        coins += val;
        coinsNumText.text = coins.ToString();
        if (val < 0) {
            PlayAudioSpendCoin();
        }
    }

    public void addArrows(int val) {
        // Add arrows to player's total and update display
        arrows += val;
        arrowsNumText.text = arrows.ToString();
    }

    public void updateHealth(int val) {
        // Manage health change based on given value


        // Adjust health
        health += val;

        // Check against health values
        if (health > maxHealth) {
            health = maxHealth;
        } else if (health <= 0 ) {
            health = 0;
        }

        // Update health display
        hpNumText.text = health.ToString() + "/" + maxHealth.ToString();
        healthBar.adjustHealth(health);
        showHideHealthBar();

        // Check for Death
        if (health <= 0) {
            Death();
        }
    }

    protected void showHideHealthBar() {
        // Do not display enemy healthbar if at maxHealth
        if (health == maxHealth) {
            hideHealthBar();
        } else {
            showHealthBar();
        }
    }

    protected void hideHealthBar() {
        healthBarTransform.localScale = new Vector3(0, 0, 0);
    }

    protected void showHealthBar() {
        healthBarTransform.localScale = new Vector3(1, 1, 1);
    }

    public void TakeDamage(int damage, int dir) {
        // damage: damage dealt
        // dir: direction from which damage was taken
        state = State.damaged;
        PlayAudioDamaged();
        updateHealth(-damage);
        Knockback(dir);
    }

    public void Knockback(int dir) {
        // Knockback effect amount based on knockbackForce
        dir = dir > 0 ? 1 : -1;
        rb.velocity = new Vector2(knockbackForce * dir, rb.velocity.y);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        // Check for triggers
        if (other.gameObject.tag == "Merchant") {
            var merchantScript = other.gameObject.GetComponent<Merchant>();
            merchantScript.PlayerInRange(true);
        }

        if (other.gameObject.tag == "Item") {
            var itemScript = other.gameObject.GetComponent<Item>();
            itemScript.collectItem();
        }

        if (other.gameObject.tag == "EnemyAttack") {
            var enemyAttackScript = other.gameObject.GetComponent<EnemyAttack>();
            enemyAttackScript.TriggerAttackOnPlayer();
        }

        if (other.gameObject.tag == "Climbable") {
            canClimb = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        // Check when leaving a merchant's trigger range
        if (other.gameObject.tag == "Merchant") {
            var merchantScript = other.gameObject.GetComponent<Merchant>();
            merchantScript.PlayerInRange(false);
        }
        if (other.gameObject.tag == "Climbable") {
            canClimb = false;
            state = State.falling;
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        // Check when colliding with an enemy object
        if (other.gameObject.tag == "Enemy") {
            if (state == State.falling) {
                var enemyScript = other.gameObject.GetComponent<Enemy>();
                enemyScript.TakeDamage(jumpOnDamage, 0);
                Jump(jumpHeight*.75f);
            } else {
                int dir = other.gameObject.transform.position.x > transform.position.x ? -1 : 1;
                TakeDamage(1, dir);
            }
        }
    }

    public void Death() {
        // If dead, play death animation and remove rigidbody/collider
        if (state != State.dead) {
            state = State.dead;
            hideHealthBar();
            anim.Play("player_death");
            audioMain.Stop();
            PlayAudioDeath();
        }
        if (rb != null) {
            rb.velocity = Vector3.zero;
            Destroy(rb);
            Destroy(coll);
            }
    }

    private bool CanJump() {
        if (coll.IsTouchingLayers(ground) && Mathf.Abs(rb.velocity.y) <= 0.5f) {
            return true;
        } else {
            return false;
        }
    }

    private void HandleMovement() {
        // Handle general movement
        float hDirection = (Input.GetAxis("Horizontal"));

        // Moving left
        if (hDirection < 0) {
            rb.velocity = new Vector2(-speed, rb.velocity.y);
            if (m_FacingRight == true) {
                Flip();
            }
        // Moving right
        } else if((hDirection > 0)) {
            rb.velocity = new Vector2(speed, rb.velocity.y);
            if (m_FacingRight == false) {
                Flip();
            }
        } else if (hDirection == 0) {
            // No x velocity if no input even while falling (slightly more mid-air control)
            rb.velocity = new Vector2(0, rb.velocity.y);
        }

        // Jumping
        if(Input.GetButtonDown("Jump") && CanJump()) {
            Jump(jumpHeight);
            PlayAudioJump();
        // Falling
        } else if (!coll.IsTouchingLayers(ground) && state != State.jumping && state != State.releasing) {
            // TODO state does not change if technically falling but still touching the side of 'ground'
            state = State.falling;
        }
    }

    private void HandleMovementWhileDamaged() {
        // Allowed movement when damamged (None, set state when stops moving)
        if (Mathf.Abs(rb.velocity.x) < .1f) {
                state = State.idle;
            }
    }

    private void HandleMovementWhileCharging() {
        // Allowed movement when charging
        // Can only change face direction and stop velocity when on the ground
        float hDirection = (Input.GetAxis("Horizontal"));

        // Moving left
        if (hDirection < 0) {
            if (m_FacingRight == true) {
                Flip();
            }
        // Moving right
        } else if((hDirection > 0)) {
            if (m_FacingRight == false) {
                Flip();
            }
        }

        // No forward velocity if no left/right input
        if (coll.IsTouchingLayers(ground)) {
            rb.velocity = Vector2.zero;
        }
    }

    private void HandleMovementWhileClimbing() {
        float vDirection = (Input.GetAxis("Vertical"));
        state = State.climbing;

        // Going up
        if (vDirection > 0) {
            rb.velocity = new Vector2(0, climbSpeed);

        // Going down
        } else if((vDirection < 0)) {
            rb.velocity = new Vector2(0, -climbSpeed);
        } else {
            rb.velocity = Vector2.zero;
        }
        if (Input.GetButtonDown("Jump")) {
            Jump(jumpHeight);
        }
        // lock pleyer to the wall (determine left/right)

    }

    private void Flip() {
		// Rotate player on Y axis to face opposite direction
		m_FacingRight = !m_FacingRight;
		transform.Rotate(0f, 180f, 0f);
	}

    private void Jump(float jumpForce) {
        // Jump, height of jump can be adjusted
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        state = State.jumping;
    }

    private void Shoot() {
        // Shoot an arrow, velocity of arrow depends on shotCharge/shotPower

        // Create arrow and specify velocity based on current shotPower
        GameObject projectile = Instantiate(projectilePrefab, firePoint.transform.position, firePoint.transform.rotation);
        Rigidbody2D projRB = projectile.GetComponent<Rigidbody2D>();
        projRB.velocity = transform.right * 10 * shotPower;
        projectile.GetComponent<Projectile>().SetDamageToDeal(shotPower + bonusDamage);
        shotPower = baseShotDamage;
        PlayAudioShoot();

        // Remove arrows from 'quiver'
        if (!infiniteArrows) {
            addArrows(-1);
        }

    }

    private void AnimationState() {
        // Determine state - bit of a mess with this + setStateAfterAnimation
        if (state != State.charging && effectIsActive == false) {
            adjustSpiteRendererColor(Color.white);
            firePointSpriteRenderer.color = Color.white;
            firePointSpriteRenderer.enabled = false;
        }

        // Update the current state based on below conditions
        if (state == State.dead) {
            // Dead
        } else if (state == State.climbing) {

        } else if (state == State.charging) {
            if (Input.GetButtonDown("Fire2")) {
                state = State.idle;
            }
            // Set fall speed to almost 0
            rb.velocity = new Vector2(rb.velocity.x*chargeFallSpeed, rb.velocity.y*chargeFallSpeed);
            //rb.velocity = Vector2.zero;
        } else if (state == State.releasing) {

        } else if (state == State.jumping) {
            if(rb.velocity.y < .1f){
                state = State.falling;
            }
        } else if (state == State.falling) {
            if(coll.IsTouchingLayers(ground)){
                if (Mathf.Abs(rb.velocity.x) > 2f) {
                    state = State.running;
                } else {
                    state = State.idle;
                }
            }
        } else if (state == State.damaged) {
            if (Mathf.Abs(rb.velocity.x) < .1f) {
                state = State.idle;
            }
        } else if (Mathf.Abs(rb.velocity.x) > 2f) {
            state = State.running;
        } else {
            state = State.idle;
        }
    }

    public void setStateAfterAnimation() {
        // Called when certain animations finish (e.g. damaged)
        if (health <= 0) {
            Death();
        } else if (rb.velocity.y < .1f) {
            state = State.falling;
        }  else if (Mathf.Abs(rb.velocity.x) > 2f) {
            state = State.running;
        } else {
            state = State.idle;
        }
    }

    public void BuyArrows(int coinCost, int numArrows) {
        // Buy arrows at coinCost if player has enough
        if (coinCost <= coins) {
            addCoins(-coinCost);
            addArrows(numArrows);
        }
    }

    public void BuyHearts(int coinCost, int numHearts) {
        // Buy hearts at coinCost if player has enough
        if (coinCost <= coins) {
            addCoins(-coinCost);
            updateHealth(numHearts);
        }
    }

    public void PlayAudioJump() {
        audioJump.Play();
    }

    public void PlayAudioShoot() {
        audioShoot.Play();
    }

    public void PlayAudioDamaged() {
        audioDamaged.Play();
    }

    public void PlayAudioDeath() {
        audioDeath.Play();
    }

    public void PlayAudioSpendCoin() {
        audioSpendCoin.Play();
    }

    public void PlayAudioFootstep() {
        // Called via Animation Event at specific points in player_walk animation
        audioFootstep.Play();
    }

    public void ShowDeathScreen() {
        // Called via Animation Event so it waits to show after the death animation plays out
        deathScreen.gameObject.SetActive(true);
    }

    public void SaveStats() {
        // Save specified stats to be loaded in another scene
        PlayerStats.Coins = coins;
        PlayerStats.Arrows = arrows;
        PlayerStats.MaxHealth = maxHealth;
    }
    public void LoadStats() {
        // Load specified stats saved from previous scenes
        coins = PlayerStats.Coins;
        arrows = PlayerStats.Arrows;
        health = PlayerStats.MaxHealth;
        maxHealth = PlayerStats.MaxHealth;
    }

    public bool ApplyEffect(GameObject incomingEffect) {
        if (effectIsActive == true) {
            return false;
        }

        effect = incomingEffect;
        Effect effectScript = incomingEffect.GetComponent<Effect>();
        var effectDuration = effectScript.GetEffectDuration();
        var effectColor = effectScript.GetEffectColor();
        adjustSpiteRendererColor(effectColor);

        // Let the effect apply to the player for specified duration
        effectScript.Apply(GetComponent<PlayerController>());
        effectIsActive = true;
        Invoke("RemoveEffect", effectDuration);

        return true;
    }

    public void RemoveEffect() {
        effectScript = effect.GetComponent<Effect>();
        effectScript.Remove(GetComponent<PlayerController>());
        effectIsActive = false;
        adjustSpiteRendererColor(Color.white);
    }

    public void AdjustSpeed(int val) {
        speed += val;
    }

    public void AdjustBonusDamage(int val) {
        bonusDamage += val;
    }

    public void AdjustChargeFallSpeed(float val) {
        chargeFallSpeed = val;
    }

    public void AdjustJumpHeight(float val) {
        jumpHeight += val;
    }
    public void AdjustJumpOnDamage(int val) {
        jumpOnDamage += val;
    }

    public void AdjustMaxHealth(int val) {
        maxHealth += val;
        updateHealth(maxHealth-health);
    }

    private void adjustSpiteRendererColor(Color color) {
        if (effectIsActive == false) {
            spriteRenderer.color = color;
        }
    }

    public void EnableInfiniteArrows(bool val) {
        infiniteArrows = val;
    }

    public void AdjustAttackRate(float val) {
        attackRate = val;
    }

    public void ResetAttackRate() {
        attackRate = baseAttackRate;
    }
}

