﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour

{   // Store and load these on scene transitions
    private static int health, maxHealth, coins, arrows;
    private static bool startAtCheckPoint = false;

    public static int Health {
        get { return health; }
        set { health = value; }
    }
    public static int MaxHealth {
        get { return maxHealth; }
        set { maxHealth = value; }
    }
    public static int Coins {
        get { return coins; }
        set { coins = value; }
    }
    public static int Arrows {
        get { return arrows; }
        set { arrows = value; }
    }
    public static bool StartAtCheckPoint {
        get { return startAtCheckPoint; }
        set { startAtCheckPoint = value; }
    }
}
