﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    // Component Vars
    public Rigidbody2D rb; // Set by player
    private Collider2D coll;
    private int damageToDeal = 1;
    [SerializeField] private GameObject player;
    [SerializeField] private LayerMask ground;

    void Start() {
        coll = GetComponent<Collider2D>();
    }

    void Update() {
        // Destroy if touching ground
        if (coll.IsTouchingLayers(ground)) {
            Destroy(gameObject);
        };
    }

    // Update is called once per frame
    private void OnCollisionEnter2D(Collision2D other) {

        // Damage enemy and destroy self
        if (other.gameObject.tag == "Enemy") {
            var enemyScript = other.gameObject.GetComponent<Enemy>();

            int dir;
            if (other.transform.position.x > transform.position.x) {
                dir = 1;
            } else {
                dir = -1;
            }
            enemyScript.TakeDamage(damageToDeal, dir);
            Destroy(gameObject);

        // Destroy if collides with another projectile
        } else if (other.gameObject.tag == "Projectile") {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        // Collect items with arrows
        if(other.tag == "Item") {
            var scr = other.gameObject.GetComponent<Item>();
            scr.collectItem();
        }

        // Destroy Spawners (restrict damage to only 1 no matter the charge?)
        if (other.tag == "Spawner") {
            var scr = other.GetComponent<Spawner>();
            scr.TakeDamage(1, 0);
        }
    }

    public void SetDamageToDeal(int val) {
        damageToDeal = val;
    }

    private void OnBecameInvisible() {
        // Destroy if out of visible range
        Destroy(gameObject, .1f);
    }
}
