﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePoint : Item
{
    public override void collectItem() {
        PlayerStats.StartAtCheckPoint = true;
        playerScript.SaveStats();
        audioCollect.Play();
        if (floatText != null) {
            floatTextScript.Show(floatTextDuration);
        }

        base.collectItem();
        Destroy(gameObject, floatTextDuration + .3f);
    }
}
