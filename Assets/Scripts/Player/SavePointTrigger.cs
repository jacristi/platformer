﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePointTrigger : MonoBehaviour
{
    [SerializeField] private GameObject pfSavePoint;

    private void Start() {
        if (PlayerStats.StartAtCheckPoint) {
            Destroy(gameObject);
        }
    }

    private void OnDestroy() {
        if (gameObject.GetComponent<Enemy>().WasKilled() && !PlayerStats.StartAtCheckPoint) {
            Instantiate(pfSavePoint, transform.position, transform.rotation);
        }

    }
}
