﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FloatText : MonoBehaviour
{
    private TextMeshPro textMesh;

    private void Awake() {
        textMesh = transform.GetComponent<TextMeshPro>();
    }

    public void Setup(string text) {
        textMesh.SetText(text.ToString());
        Hide();
    }

    private void Update() {
        if (Input.GetKey(KeyCode.LeftAlt)) {
            textMesh.enabled = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftAlt)) {
            textMesh.enabled = false;
        }
    }

    public void Show(float dur) {
        // Show the text for dur time
        textMesh.enabled = true;
        Invoke("Hide", dur);
    }

    public void Hide() {
        textMesh.enabled = false;
    }
}
