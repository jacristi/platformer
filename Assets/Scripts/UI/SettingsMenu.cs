﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SettingsMenu : MonoBehaviour
{

    public AudioMixer audioMixer;

    public void SetMasterVolumeLevel(float volume) {
        audioMixer.SetFloat("MasterVolume", Mathf.Log10(volume)*20);
    }

    public void SetMusicVolumeLevel(float volume) {
        audioMixer.SetFloat("MusicVolume", Mathf.Log10(volume)*20);
    }

    public void SetSFXVolumeLevel(float volume) {
        audioMixer.SetFloat("SFXVolume", Mathf.Log10(volume)*20);
    }
}
