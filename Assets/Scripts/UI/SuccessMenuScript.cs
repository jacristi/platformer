﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SuccessMenuScript : MonoBehaviour
{
    public void MainMenu() {
        SceneManager.LoadScene(0);
    }
    public void Restart() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
    public void ExitGame() {
        Application.Quit();
    }
}
