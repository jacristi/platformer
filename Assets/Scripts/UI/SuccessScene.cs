﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SuccessScene : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI coinsNumText;
    [SerializeField] private TextMeshProUGUI arrowsNumText;

    private void Awake() {
        PlayerStats.StartAtCheckPoint = false;
    }

    void Start()
    {
        coinsNumText.text = PlayerStats.Coins.ToString();
        arrowsNumText.text = PlayerStats.Arrows.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
